# UVCCapture

### 介绍

UVCCapture是windows下利用DirectShow和qt支持的摄像头拍照软件，支持使用摄像头的硬件按钮触发拍照功能。虽然qt的QCamera已经支持了摄像，拍照等功能，但是由于不支持使用摄像头的硬件按钮触发拍照，所以本软件基于direct show实现了这个功能。

### 软件架构

##### 开发环境

VS2022

##### 依赖工具包

qt5.15.5

### 安装教程

##### 编译安装qt

由于本工程非常小，使用qt5的时候可以静态编译qt，形成一个独立的exe文件，可以使用如下所示的configure选项：

``` bash
.\configure.bat -static -developer-build -debug-and-release -platform win32-msvc -prefix "D:\qt\qt-5.15.5-msvc2022-x86-static-full" -opensource -confirm-license -nomake examples -nomake tests -skip qtwebengine -qt-zlib -qt-libpng -qt-libjpeg -qt-freetype -qt-pcre -qt-harfbuzz -opengl dynamic -mp
```

##### 编译工程

编译工程时，打开CMD，输入qmake即可生成VS工程：

``` bash
d:\qt\qt-5.15.5-msvc2022-x86-static-full\bin\qmake -tp vc
```

##### 更新翻译文件

需要更新中文翻译文件，请修改translation/zh.ts文件，并且使用lupdate、lrelease更新zh.ts以及zh.pm：

``` bash
d:\qt\qt-5.15.5-msvc2022-x86-static-full\bin\lupdate uvc_capture.pro
d:\qt\qt-5.15.5-msvc2022-x86-static-full\bin\lrelease uvc_capture.pro
```

翻译文件格式可以手动修改，各个字段类似与下边的XML代码：

``` xml
<message>
    <location filename="../WebcamWindow.cpp" line="46"/>
    <source>Resolutions</source>
    <translation>分辨率</translation>
</message>
<message>
    <location filename="../WebcamWindow.cpp" line="48"/>
    <source>Output Path</source>
    <translation>输出路径</translation>
</message>
```

### 支持功能

1. 软件按钮点击录制MJPG编码AVI视频
2. 硬件按钮触发拍照
3. 软件按钮点击拍照
4. 选择分辨率和设备
由于连续按两次硬件按钮触发摄像功能不稳定，默认已经移除
