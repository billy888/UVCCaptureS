TEMPLATE = app
TARGET = UVCapture
SOURCES += main.cpp ImageFormats.cpp VideoCapture.cpp VideoDevice.cpp WebcamWindow.cpp AVILib.cpp Logger.cpp

HEADERS += ImageFormats.h SampleGrabber.h VideoCapture.h VideoCaptureCallback.h VideoDevice.h WebcamWindow.h AVILib.h Logger.h

QT += core gui widgets texttospeech

# multimediawidgets multimedia

TRANSLATIONS = translation/zh.ts

RC_FILE = uvc_capture.rc

INCLUDEPATH += ./lib/include

contains(QMAKE_TARGET.arch, x86_64) {

debug {
}
release {
}

} else {

}
